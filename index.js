/**
 * This javascript file will constitute the entry point of your solution.
 *
 * Edit it as you need.  It currently contains things that you might find helpful to get started.
 */

//This is not really required, but means that changes to index.html will cause a reload.
require('./site/index.html')
//Apply the styles in style.css to the page.
require('./site/style.css')

//if you want to use es6, you can do something like
//require('./es6/myEs6code')
//here to load the myEs6code.js file, and it will be automatically transpiled.

//Change this to get detailed logging from the stomp library
global.DEBUG = false

const url = "ws://localhost:8011/stomp"
	const client = Stomp.client(url)
	client.debug = function(msg) {
	if (global.DEBUG) {
		console.info(msg)
	}
}
//This is a contract between the server and the client that the Object names are defined and only other attributes is anticipated to change which the UI will update to.
let priceList = [ {
	"name" : "gbpusd",

},
{
	"name" : "gbpeur",

},
{
	"name" : "gbpaud",

},
{
	"name" : "usdeur",

},
{
	"name" : "gbpjpy",

},
{
	"name" : "usdjpy",

},
{
	"name" : "eurjpy",

},
{
	"name" : "gbpchf",

},
{
	"name" : "euraud",

},
{
	"name" : "eurchf",

},
{
	"name" : "eurcad",


},
{
	"name" : "gbpcad",

} ];


//Callback invoked after every message recieved from the Stomp
function connectCallback() {
	client.subscribe("/fx/prices", function(message) {
		var quote = JSON.parse(message.body);
		//Attaching the update to the respective model.

		priceList.forEach((val, i) => {
			if (val.name == quote.name) {
				priceList[i] = {
						"name" : quote.name,
						"bestBid" : quote.bestBid,
						"bestAsk" : quote.bestAsk,
						"lastChangeBid" : quote.lastChangeBid,
						"lastChangeAsk" : quote.lastChangeAsk
				};

			}

		})
		//Price list remain sorted from low to high in ascending order on lastChangeBid
		priceList.sort(function(a, b) {
			return a.lastChangeBid - b.lastChangeBid
		});
		createTableDynamically();

		document.getElementById('stomp-msg').innerHTML = "Price changed for entity name " + quote.name;
	});

}
//creating table and updating table for respective messages coming from Stomp.
function createTableDynamically() {
	var objLength = priceList.length;
	const coulumnHeaders=['Name','Best Bid Price','Best Ask Price','Last Changed Best Bid','Last Changed Best Ask Price','Sparkline'];


	const myvar = `<table style= "width: 100%;">
		<tr>
		<th>${coulumnHeaders[0]}</th>
		<th>${coulumnHeaders[1]}</th>
		<th>${coulumnHeaders[2]}</th>
		<th>${coulumnHeaders[3]}</th>
		<th>${coulumnHeaders[4]}</th>
		<th>${coulumnHeaders[5]}</th>
	</tr>
	${priceList.map(item=>

	`<tr>
	<td > ${item.name?item.name:''} </td>
	<td> ${item.bestBid?item.bestBid:''} </td>
	<td> ${ item.bestAsk?item.bestAsk:''}</td>
	<td style="min-width:210px">${item.lastChangeBid?item.lastChangeBid:''}</td> 
	<td style="min-width:230px"> ${item.lastChangeAsk?item.lastChangeAsk:''} </td> 
	<td style="min-width:100px"></td>
	</tr>`
	).join('')}
	</table>`;
	document.getElementById('myTable').innerHTML = myvar;
	//By Default blank and will update after 30s.



	}
	//	Price List gets updated with each stomp message so the average is calculated on the updated values.
	setInterval(()=>{
	updateSparklineForEach();
	},3000)
	function updateSparklineForEach() {
	var sparklines = document.querySelectorAll('td:nth-child(6)');
	sparkLineVals = [];


	for (var i = 0; i < sparklines.length; i++) {
	var avg = (priceList[i].bestBid + priceList[i].bestAsk) / 2;
	sparkLineVals.push(avg);
	Sparkline.draw(sparklines[i], sparkLineVals)
	}

	}


	client.connect({}, connectCallback, function(error) {
	alert(error.headers.message)
	})